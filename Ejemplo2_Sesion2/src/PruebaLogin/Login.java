package PruebaLogin;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class Login extends JFrame implements ActionListener {
	
	private JLabel logo;
	private JTextField user;
	private JPasswordField pass;
	private JButton limpiar, aceptar;
	
	private String miuser="miusuario", mipass="micontraseña";

	public Login() {
		// TODO Auto-generated constructor stub
		super("Log in - Mi Sistema");
		setLayout(new FlowLayout(FlowLayout.LEFT));
		
		logo = new JLabel("Login_Corporacion");
		user = new JTextField(30);
		pass = new JPasswordField(30);
		limpiar = new JButton("Borrar");
		aceptar = new JButton("Ingresar");
		
		limpiar.addActionListener(this);
		aceptar.addActionListener(this);
		
		add(logo);
		add(user);
		add(pass);
		add(limpiar);
		add(aceptar);
	}

	public static void main(String[]args) {
		Login log = new Login();
		log.setSize(350, 200);
		log.setVisible(true);
		log.setResizable(false);
		log.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		if(arg0.getSource() == limpiar) {
			user.setText("");
			pass.setText("");
		}else if(arg0.getSource() == aceptar) {
			if((user.getText() == miuser)&&(pass.getPassword().equals(mipass))) {
				JOptionPane.showMessageDialog(null, "Login Correcto");
			}else {
				JOptionPane.showMessageDialog(null, "Login Incorrecto "+ pass.getPassword().toString());
			}
		}
	}
}
