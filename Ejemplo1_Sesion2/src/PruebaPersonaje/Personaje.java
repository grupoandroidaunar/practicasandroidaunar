package PruebaPersonaje;

import java.util.Scanner;


public class Personaje {
	public static Scanner entrada;
	private String color;

	public Personaje() {
		// TODO Auto-generated constructor stub
		
		color = "Verde";
		System.out.println("Androide Color: " +color);
	}
	
	private void saltar() {
		System.out.println("Salto");
	}
	
	private void bajar() {
		System.out.println("Bajo");
	}

	private void avanzar() {
		System.out.println("Adelante");
	}
	
	private void retroceder() {
		System.out.println("Atras");
	}
	
	private void giro_izq() {
		System.out.println("Giro Izq");
	}
	
	private void giro_der() {
		System.out.println("Giro Der");
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Personaje androide;
		androide = new Personaje();
		
		entrada = new Scanner(System.in);
		
		char comando;
		
		while(true) {
			System.out.print("Comando: ");
			comando = entrada.next().charAt(0);
			if((comando == 'w')||(comando == 'W')) {
				androide.avanzar();
			}else if((comando == 's')||(comando == 'S')) {
				androide.retroceder();
			}else if((comando == 'a')||(comando == 'A')) {
				androide.giro_izq();
			}else if((comando == 'd')||(comando == 'D')) {
				androide.giro_der();
			}else if((comando == 'v')||(comando == 'V')) {
				androide.saltar();
			}else if((comando == 'c')||(comando == 'C')) {
				androide.bajar();
			}
		}
		
	}

}
